package com.flora.michael.shop.ui.main

import androidx.lifecycle.ViewModelProvider
import com.flora.michael.shop.R
import com.flora.michael.shop.ui.abstracted.activity.BaseActivity

class MainActivity : BaseActivity() {
    private val tag = this::class.java.simpleName
    private val viewModel by lazy {
        ViewModelProvider(this)[MainViewModel::class.java]
    }
    override val layoutResource = R.layout.activity_main

}