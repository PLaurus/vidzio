package com.flora.michael.shop

import android.app.Application
import android.util.Log

class ShopApplication : Application() {
    private val tag = this::class.java.simpleName

    override fun onCreate() {
        super.onCreate()
        Log.v(tag, "Application launched")
    }
}