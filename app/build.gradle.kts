/**
 * Applies the Android plugin for Gradle to this build and makes the android block available to
 * specify Android-specific build options
 */
plugins{
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

/**
 * The android block allows to configure all Android-specific build options
 */
android {
    val compile_sdk_version = 29
    val build_tools_version = "29.0.2"
    val min_sdk_version = 21
    val target_sdk_version = 29

    /**
     * compileSdkVersion specifies the Android API level Gradle should use to compile
     * the application. The application can use the API features included in
     * this API level and lower
     */
    compileSdkVersion(compile_sdk_version)

    /**
     * buildToolsVersion specifies the version of the SDK build tools and compiler that Gradle
     * should use to build the app (Optional)
     */
    buildToolsVersion(build_tools_version)

    /**
     * The defaultConfig block encapsulates default settings and entries for all
     * build variants, and can override some attributes in main/AndroidManifest.xml
     * dynamically from the build system. Product flavors can override
     * these values for different versions of the app.
     */
    defaultConfig {
        //applicationId uniquely identifies the package for publishing
        applicationId = "com.flora.michael.shop"

        //Defines the minimum API level required to run the application
        minSdkVersion(min_sdk_version)

        //Specifies the API level used to test the application
        targetSdkVersion(target_sdk_version)

        //Defines the current version number of the application
        versionCode = 1

        //Defines the current user-friendly version name for the application
        versionName = "0.0.1"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    //The buildTypes block specifies multiple build types. By default are debug and release.
    buildTypes{
        getByName("release"){
            //Enables code shrinking when set in true
            isMinifyEnabled = true

            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }

        getByName("debug"){
            applicationIdSuffix = ".debug"

            //Enables code shrinking when set in true
            isMinifyEnabled = false
        }
    }

    compileOptions{
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    sourceSets{
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("test").java.srcDirs("src/test/kotlin")
        getByName("androidTest").java.srcDirs("src/androidTest/kotlin")
    }
}

/**
 * The dependencies block in the module-level build config file specifies dependencies required
 * to build only this module
 */
dependencies {
    val kotlinVersion = "1.3.50"
    val coroutinesVersion = "1.3.1"

    val coreKtxVersion = "1.2.0-alpha04"

    val materialDesignVersion = "1.0.0"
    val appCompatVersion = "1.1.0"
    val recyclerviewVersion = "1.0.0"

    val lifecycleVersion = "2.2.0-alpha05"

    val gsonVersion = "2.8.5"
    val kotsonVersion = "2.5.0"

    val roomVersion = "2.1.0"

    val okhttpVersion = "3.10.0"
    val okhttpLoggingInterceptorVersion = "3.9.0"
    val retrofitVersion = "2.3.0"
    val gsonConverterVersion = "2.0.2"

    // Kotlin
    implementation(kotlin("stdlib-jdk8", kotlinVersion))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")

    // AndroidX
    implementation("androidx.core:core-ktx:$coreKtxVersion")

    // UI
    implementation("com.google.android.material:material:$materialDesignVersion")
    implementation("androidx.appcompat:appcompat:$appCompatVersion")
    implementation("androidx.recyclerview:recyclerview:$recyclerviewVersion")

    // Lifecycle
    kapt("androidx.lifecycle:lifecycle-compiler:$lifecycleVersion")
    implementation("androidx.lifecycle:lifecycle-extensions:$lifecycleVersion")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:$lifecycleVersion")

    // Serialization
    implementation("com.google.code.gson:gson:$gsonVersion")
    implementation("com.github.salomonbrys.kotson:kotson:$kotsonVersion")

    // Room
    implementation("androidx.room:room-ktx:$roomVersion")
    kapt("androidx.room:room-compiler:$roomVersion")

    //HTTP Client
    implementation("com.squareup.okhttp3:okhttp:$okhttpVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:$okhttpLoggingInterceptorVersion")
    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-gson:$gsonConverterVersion")

    //Tests
    testImplementation("junit:junit:4.12")
    androidTestImplementation("androidx.test:runner:1.2.0")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0")
}

//android{
//    /**
//     * The productFlavors block intended to configure multiple product flavors.
//     * Allows to override the defaultConfig block's settings
//     */
//    productFlavors{
//        create("free"){
//            applicationId = "com.flora.michael.shop.free"
//        }
//
//        getByName("paid"){
//            applicationId = "com.flora.michael.shop.paid"
//        }
//    }
//}

//dependencies{
//    //Dependency on local libraries
//    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
//
//    //Dependency on a local library module
//    implementation(project(":myLibrary"))
//}

