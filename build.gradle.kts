/** Top-level build file where you can add configuration options common to all sub-projects/modules.*/

/**
 * The buildscript block is used to define the repositories and dependencies for Gradle itself-meaning.
 * The block shouldn't contain dependencies for an app's modules!
 */
buildscript{
    val gradle_plugin_version = "3.5.0"
    val kotlin_version = "1.3.50"

    /**
     * The repositories block configures the repositories Gradle uses to
     * search or download the dependencies.
     */
    repositories{
        google()
        jcenter()
        mavenCentral()
    }

    /**
     * The dependencies block configures the dependencies Gradle needs to use
     * to build the project.
     */
    dependencies{
        classpath("com.android.tools.build:gradle:$gradle_plugin_version")
        classpath(kotlin("gradle-plugin", version = kotlin_version))
    }
}

/**
 * The allprojects block configures the repositories and
 * dependencies used by all modules in the project, such as third-party plugins
 * or libraries. Module-specific dependencies should be placed in
 * each module-level build.gradle file!
 */
allprojects{

    /**
     * The repositories block configures the repositories Gradle uses to
     * search or download the dependencies.
     */
    repositories {
        google()
        jcenter()
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
