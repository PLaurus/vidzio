/**Which modules should be included when building the app.
 * Multi-module projects need to specify each module that should go into the final build
 */

include(":app")
rootProject.name = "Shop"
